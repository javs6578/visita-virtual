import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StepperComponent } from './components/stepper/stepper.component';
import { AppRoutingModule } from './app-routing.module';
import { MarriedShareholderComponent } from './components/married-shareholder/married-shareholder.component';
import { QuestionShareholderComponent } from './components/question-shareholder/question-shareholder.component';
import { QrShareholderComponent } from './components/qr-shareholder/qr-shareholder.component';
import { EmailShareholderComponent } from './components/email-shareholder/email-shareholder.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StepperComponent,
    MarriedShareholderComponent,
    QuestionShareholderComponent,
    QrShareholderComponent,
    EmailShareholderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
