import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MarriedShareholderComponent } from './components/married-shareholder/married-shareholder.component';
import { EmailShareholderComponent } from './components/email-shareholder/email-shareholder.component';
import { QuestionShareholderComponent } from './components/question-shareholder/question-shareholder.component';
import { QrShareholderComponent } from './components/qr-shareholder/qr-shareholder.component';

const routes: Routes = [
  {
    path: 'married-shareholder',
    component: MarriedShareholderComponent
  },
  {
    path: 'qr-shareholder',
    component: QrShareholderComponent
  },
  {
    path: 'question-shareholder',
    component: QuestionShareholderComponent
  },
  {
    path: 'email-shareholder',
    component: EmailShareholderComponent
  },
  {
    path: '**',
    component: MarriedShareholderComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
