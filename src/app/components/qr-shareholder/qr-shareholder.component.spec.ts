import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QrShareholderComponent } from './qr-shareholder.component';

describe('QrShareholderComponent', () => {
  let component: QrShareholderComponent;
  let fixture: ComponentFixture<QrShareholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QrShareholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QrShareholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
