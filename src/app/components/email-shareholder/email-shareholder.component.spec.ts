import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailShareholderComponent } from './email-shareholder.component';

describe('EmailShareholderComponent', () => {
  let component: EmailShareholderComponent;
  let fixture: ComponentFixture<EmailShareholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailShareholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailShareholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
