import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionShareholderComponent } from './question-shareholder.component';

describe('QuestionShareholderComponent', () => {
  let component: QuestionShareholderComponent;
  let fixture: ComponentFixture<QuestionShareholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionShareholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionShareholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
