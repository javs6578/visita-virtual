import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarriedShareholderComponent } from './married-shareholder.component';

describe('MarriedShareholderComponent', () => {
  let component: MarriedShareholderComponent;
  let fixture: ComponentFixture<MarriedShareholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarriedShareholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarriedShareholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
